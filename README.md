# NATO Phonetic Alphabet Spelling

natoword is a Python script that will take a list of words and produce their
spellings using the NATO alphabet.

For example, the world `Hello` would be spelled out as

```
HOTEL echo lima lima oscar
```

Numbers will be left as-is, while the most common special characters will be
replaced with a description. See the examples below for details.

This tool is especially useful for displaying or relaying passwords in a clear
manner. Especially when passwords have similar or ambigious characters.

## Requirements

This script requires Python 3.8.

No external or third party modules are required, so only Python 3.8 is required. It should also run the same on Windows, MacOS, and Linux.

### Development

For developing further, and testing, use Poetry to install all the development requirements.

```
poetry install
```

And to test:

```
nox
```

This project uses Black to format the code, and this is invoked by nox on each run.

## Conversions

### Letters

As mentioned, alphabetical characters will be converted to their NATO equivalent.
An uppercase character will be converted to the uppercase version.

For example `a` would become `alpha`, and `A` would become `ALPHA`.

### Numbers

Numbers are not converted and left as-is.

### Special Characters

Most common special characters are converted to a description in angle brackets.

`!` would become `<exclamation>`   
`%` would become `<percent>`

Not all special characters will be converted. These will be left as-is.

## Examples

### CLI
Convert a single word:

```
natoword.py P4ssw0rd!
```

Output:
```
WORD                PHONETIC
----                --------
P4ssw0rd!           PAPA 4 sierra sierra whiskey 0 romeo delta <exclamation>
```

Convert multiple words:

```
natoword.py P4ssw0rd! s3cr3t
```

Output:
```
WORD                PHONETIC
----                --------
P4ssw0rd!           PAPA 4 sierra sierra whiskey 0 romeo delta <exclamation>
s3cr3t              sierra 3 charlie romeo 3 tango
```

### Text File
Supply word list from text file:

```
natoword.py --file my-word-list.txt
```

Output:
```
WORD                PHONETIC
----                --------
Hello               HOTEL echo lima lima oscar
World!              WHISKEY oscar romeo lima delta <exclamation>
```

There should be a single word per line. Empty lines are ignored.

### CSV File
Supply word list from CSV file:

```
natoword.py --csv my-word-list.csv
```

Output:
```
WORD                PHONETIC
----                --------
apple               alpha papa papa lima echo
banana              bravo alpha november alpha november alpha
```

The CSV must have a header row.

By default the first column of the CSV is used for the list of words. This can be
overriden using the `--csvcol` option:

```
natoword.py --csv my-word-list.csv --csvcol 2
```

Output
```
WORD                PHONETIC
----                --------
cow                 charlie oscar whiskey
sheep               sierra hotel echo echo papa
```

`--csvcol` will accept the column name or column number. Columns are indexed
from the left starting at 1.

### Output to CSV

The script can output the results to a CSV file.

It will produce two columns, `word` and `phonetic`.

```
natoword.py --outcsv my-new.csv word1 word2
```

Would write the CSV to `my-new.csv`:

```
word,phonetic
word1,whiskey oscar romeo delta 1
word2,whiskey oscar remeo delta 2
```

If the word list was provided by a CSV, the output CSV will include the original
data from it.

Original CSV:

```
user,password
morpheus,M4ch33n!
trinity,D0DG3this
```

```
natoword.py --csv input.csv --outcsv output.csv
```

Output CSV:
```
user,password,phonetic
morpheus,M4ch33n!,MIKE 4 charlie hotel 3 3 november <exclamation>
trinity,D0DG3this,DELTA 0 DELTA GOLF 3 tango hotel india sierra
```






