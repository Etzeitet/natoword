#!/usr/bin/env python3
# MIT License
#
# Copyright (c) 2020 Peter Spain
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# A simple script for "spelling" out one or more words with
# the NATO phonetic alphabet.
#
# Usage:
#
#
#    # simple word
#    natoword.py hello
#
#    >>> hotel echo lima lima oscar
#
#    # Uppercase characters:
#    natoword.py HellO
#
#    >>> HOTEL echo lima lima OSCAR
#
#    # Numbers
#    natoword.py H3ll0
#    >>> HOTEL 3 lima lima 0
#
#    # Special characters are left as-is, unless ambiguous:
#    natoword.py Hi|!
#
#    >>> HOTEL indigo <pipe> !
#
#    # Multiple words:
#    natoword.py Hello World!
#
#    >>> HOTEL echo lima lima oscar
#    >>> WHISKEY oscar romeo lima delta !
#
#    # From and/or to CSV
#    natoword.py --csv words.csv --out natowords.csv
#
#    # specify which column in CSV to read:
#    natoword.py --csv words.csv --col passwords --out natowords.csv
#
#    # read from a file
#    natoword.py --file words.txt
#
import argparse
import pathlib
import csv
import itertools

VERSION = "0.11.1"

_NATO_ALPHABET = {
    "A": "ALPHA",
    "B": "BRAVO",
    "C": "CHARLIE",
    "D": "DELTA",
    "E": "ECHO",
    "F": "FOXTROT",
    "G": "GOLF",
    "H": "HOTEL",
    "I": "INDIA",
    "J": "JULIET",
    "K": "KILO",
    "L": "LIMA",
    "M": "MIKE",
    "N": "NOVEMBER",
    "O": "OSCAR",
    "P": "PAPA",
    "Q": "QUEBEC",
    "R": "ROMEO",
    "S": "SIERRA",
    "T": "TANGO",
    "U": "UNIFORM",
    "V": "VICTOR",
    "W": "WHISKEY",
    "X": "XRAY",
    "Y": "YANKEE",
    "Z": "ZULU",
}


_SPECIAL_CHARS = {
    "!": "<exclamation>",
    '"': "<double-quote>",
    "£": "<pound>",
    "$": "<dollar>",
    "%": "<percent>",
    "^": "<caret>",
    "&": "<ampersand>",
    "*": "<asterisk>",
    " ": "<space>",
    "-": "<hyphen>",
    "_": "<underscore>",
    "+": "<plus>",
    "=": "<equals>",
    "(": "<open-parenthesis>",
    ")": "<close-parenthesis>",
    "{": "<open-curly-bracket>",
    "}": "<close-curly-bracket>",
    "[": "<open-square-bracket>",
    "]": "<close-square-bracket>",
    "<": "<open-angle-bracket>",
    ">": "<close-angle-bracket>",
    "~": "<tilde>",
    "#": "<hash>",
    ":": "<colon>",
    ";": "<semi-colon>",
    "@": "<at>",
    "'": "<single-quote>",
    "?": "<question-mark>",
    "/": "<forward-slash>",
    "\\": "<back-slash>",
    ".": "<period>",
    ",": "<comma>",
    "|": "<pipe>",
}


def convert_characters(word):
    """
    Converts each character in a sequence to NATO phonetic equivalent.

    Args
    ====
    word:   str, the word to "spell"

    Returns
    =======
    list, containing each spelling in order.

    """
    if not word:
        raise ValueError("word cannot be empty.")

    spellings = []

    lower = False
    for char in word:

        lower = char.islower()

        try:
            if char.isnumeric():
                spelling = char

            elif char in _SPECIAL_CHARS:
                spelling = _SPECIAL_CHARS[char]

            else:
                spelling = _NATO_ALPHABET[char.upper()]

                if lower:
                    spelling = spelling.lower()

        except KeyError:
            spelling = char

        spellings.append(spelling)

    return spellings


def convert_words(words):
    """
    Converts each word to NATO phonetic equivalent

    Args
    ====
    words   list, list of words to convert

    Returns
    =======
    lists of dicts. Key is the word, and the value is the list of spellings
    """

    spellings = []

    for word in words:

        try:
            phonetic = " ".join(convert_characters(word))
        except ValueError:
            continue

        spelling = {"word": word, "phonetic": phonetic}

        spellings.append(spelling)

    return spellings


def get_words_from_file(filepath):
    """
    Retrieves a list of words from a text file. Empty
    lines are skipped

    Args
    ====
    filepath:   str, Path a filepath to the text file

    Returns
    =======
    list of words
    """
    if not isinstance(filepath, pathlib.Path):
        filepath = pathlib.Path(filepath)

    if not filepath.is_file():
        raise ValueError(f"{filepath} is not a file!")

    words = None
    with open(filepath) as f:
        words = f.read().splitlines()

    return [word for word in words if word]


def get_words_from_csv(csvpath, col=1):
    """
    Retreives a list of words from a column in a CSV file.

    Unless provided, the first column will be used to grab the list
    of words. The CSV file is assumed to have row 1 as the header.

    Args
    ====
    csvpath     str, Path a filepath to the CSV file
    col     str, optional the name or number of the column.

    Returns
    =======
    A list of words.
    """

    if not isinstance(csvpath, pathlib.Path):
        filepath = pathlib.Path(csvpath)

    if not filepath.is_file():
        raise ValueError(f"{csvpath} is not a file!")

    words = []
    with open(csvpath) as f:
        csv_reader = csv.DictReader(f)

        if col in csv_reader.fieldnames:
            key = col
        else:
            key = csv_reader.fieldnames[int(col) - 1]

        for row in csv_reader:
            words.append(row[key])

    return words


def write_csv(spellings, outpath):
    """
    Writes converted spellings to CSV.

    Args
    ====
    spellings:  list of dicts with converted spellings
    outpath:    str, path to write CSV file - must not exist!
    """

    if not isinstance(outpath, pathlib.Path):
        outpath = pathlib.Path(outpath)

    if outpath.exists():
        raise ValueError(f"{outpath} already exists")

    with open(outpath, "w") as out_f:
        csv_writer = csv.DictWriter(out_f, ["word", "phonetic"], extrasaction="ignore")
        csv_writer.writeheader()
        csv_writer.writerows(spellings)


def write_csv_with_source(spellings, outpath, source_csv):
    """
    Writes converted spellings to CSV. If provided, source_csv will be used to
    create output CSV.

    Args
    ====
    spellings:  list of dicts with converted spellings
    outpath:    str, path to write CSV file - must not exist!
    source_csv: str, path to existing CSV to use as basis for new CSV

    """

    if not isinstance(outpath, pathlib.Path):
        outpath = pathlib.Path(outpath)

    if not isinstance(source_csv, pathlib.Path):
        source_csv = pathlib.Path(source_csv)

    if outpath.exists():
        raise ValueError(f"{outpath} already exists")

    if not source_csv.exists():
        raise ValueError(f"{source_csv} does not exist")

    with open(outpath, "w") as out_f, open(source_csv, "r") as in_f:
        csv_reader = csv.DictReader(in_f)

        headers = csv_reader.fieldnames
        headers.append("phonetic")

        csv_writer = csv.DictWriter(out_f, headers, extrasaction="ignore")
        csv_writer.writeheader()

        for source_row, spelling in itertools.zip_longest(csv_reader, spellings):
            source_row.update(spelling)
            csv_writer.writerow(source_row)


def display_conversions(spellings):
    """
    Prints the converted words and their new spellings to the console.

    Args
    ====
    spellings:  dict, a dict of each word as the key and the spellings
                as a list for the value
    """

    print("WORD                PHONETIC")
    print("----                --------")
    row_t = "{word:<20}{phonetic}"

    for spelling in spellings:
        row = row_t.format(**spelling)

        print(row)


def main(words=None, csvpath=None, col=1, filepath=None, outcsv=None):
    """
    Main function.

    At least one of words, csv, and filepath must be specified. If more than
    one is specified the first none-empty argument from left to right will be
    used: words, csv, filepath.

    When specifying a CSV file by default the first column will be used, unless
    col is specified. The name or column number can be provided. Columns are
    indexed left to right starting at 1. The input CSV is assumed to have row 1
    as the header. Headers will be treated as case-sensitive.

    The filepath must point to plain text file, with a word on each line.
    Empty lines are ignored.

    If outcsv is not specified, everything is printed to the console. If the
    words were provided via CSV file, outcsv will create a copy of this file
    with an extra Phonetic column.

    Args
    ====
    words       list of words to convert
    csvpath         path to CSV file with words to convert
    col         the column name or number to read from the CSV
    filepath    path to a plaintext file with words to convert
    outcsv      path to write an updated CSV file
    """

    if not any((words, csvpath, filepath)):
        raise ValueError(
            "At least one of words, csvpath, and filepath arguments must be set."
        )

    word_list = []
    if words:
        word_list = words

    elif csvpath:
        word_list = get_words_from_csv(csvpath, col)

    elif filepath:
        word_list = get_words_from_file(filepath)

    if spellings := convert_words(word_list):
        display_conversions(spellings)

        if outcsv and not words and not filepath:
            if csvpath:
                write_csv_with_source(spellings, outcsv, csvpath)
            else:
                write_csv(spellings, outcsv)


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        "natoword.py", description=f"natoword.py v{VERSION}"
    )
    parser.add_argument("--csv", dest="csvpath", help="Path to CSV file.")
    parser.add_argument("--outcsv", help="Path to output CSV")
    parser.add_argument(
        "--csvcol",
        dest="col",
        default=1,
        help="Name or index of CSV column from which to collect words. Defaults to column 1",  # noqa E501
    )
    parser.add_argument(
        "--file",
        "-f",
        dest="filepath",
        help="Path to plain text file containing words.",
    )
    parser.add_argument("words", nargs="*", help="One or more space separated words.")

    config = parser.parse_args()

    try:
        main(**vars(config))
    except ValueError:
        print("Please provide at least one word to convert.\n")
        parser.print_help()
