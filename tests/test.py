import pytest

from hypothesis import given
import hypothesis.strategies as st

from natoword import (
    convert_characters,
    convert_words,
    get_words_from_csv,
    get_words_from_file,
    write_csv,
    write_csv_with_source,
    display_conversions,
)


@pytest.mark.parametrize(
    "test_input,expected",
    [
        ("apple", ["alpha", "papa", "papa", "lima", "echo"]),
        ("APPLE", ["ALPHA", "PAPA", "PAPA", "LIMA", "ECHO"]),
        (
            "P4sSw0rd!",
            [
                "PAPA",
                "4",
                "sierra",
                "SIERRA",
                "whiskey",
                "0",
                "romeo",
                "delta",
                "<exclamation>",
            ],
        ),
        ("¬pass", ["¬", "papa", "alpha", "sierra", "sierra"]),
    ],
)
def test_convert_characters(test_input, expected):
    """
    Test convert_characters() function.
    """

    spelling = convert_characters(test_input)

    assert spelling == expected


def test_convert_characters_empty_string():
    with pytest.raises(ValueError):
        convert_characters("")


@given(st.text(min_size=1))
def test_convert_characters_hypothesis(word):

    spelling = convert_characters(word)

    assert len(word) == len(spelling)

    for i, c in enumerate(word):
        if c.isalpha():
            # letters should be converted to word starting with same letter
            assert spelling[i].startswith(c)

        elif c.isnumeric():
            # numbers should be left as-is
            assert c == spelling[i]

        else:
            # symbols will either be left as-is or replaced with <symbol name>
            assert c == spelling[i] or spelling[i].startswith("<")


@pytest.mark.parametrize(
    "test_input,expected",
    [
        (
            ["apple", "APPLE", "P4sSw0rd!", "¬pass"],
            [
                {"word": "apple", "phonetic": "alpha papa papa lima echo"},
                {"word": "APPLE", "phonetic": "ALPHA PAPA PAPA LIMA ECHO"},
                {
                    "word": "P4sSw0rd!",
                    "phonetic": "PAPA 4 sierra SIERRA whiskey 0 romeo delta <exclamation>",  # noqa E501
                },
                {"word": "¬pass", "phonetic": "¬ papa alpha sierra sierra"},
            ],
        ),
    ],
)
def test_convert_words(test_input, expected):
    """
    Test convert_words() function
    """

    spellings = convert_words(test_input)

    assert spellings == expected


@given(st.lists(st.text()))
def test_convert_words_hypothesis(words):
    spellings = convert_words(words)

    words_without_empties = [x for x in words if x]
    assert len(spellings) == len(words_without_empties)


@pytest.mark.parametrize(
    "csv_path,column,expected",
    [
        ("tests/input.csv", 1, ["neo", "asmith", "morpheus", "syfer"]),
        ("tests/input.csv", "2", ["Th30ne", "1nevitabl3", "M4cheenZ", "St3aK!"]),
        ("tests/input.csv", "password", ["Th30ne", "1nevitabl3", "M4cheenZ", "St3aK!"]),
    ],
)
def test_get_words_from_csv(csv_path, column, expected):
    """
    Test get_words_from_csv() function.
    """

    words = get_words_from_csv(csv_path, column)

    assert words == expected


@pytest.mark.parametrize(
    "file_path,expected",
    [("tests/test.txt", ["apple", "banana", "cherry", "sheep", "cow", "horse"])],
)
def test_get_words_from_file(file_path, expected):
    """
    Test get_words_from_file() function.
    """

    words = get_words_from_file(file_path)

    assert words == expected


@pytest.mark.parametrize(
    "reference_path,input_csv_path", [("tests/csv_reference.csv", "tests/input.csv")],
)
def test_output_csv(reference_path, input_csv_path, tmp_path):
    """
    Tests write_csv() function.
    """

    words = get_words_from_csv(input_csv_path, 2)
    spellings = convert_words(words)

    test_path = tmp_path.joinpath("output.csv")

    # pdb.set_trace()
    write_csv(spellings, test_path)

    with open(reference_path) as ref_f, open(test_path) as test_f:
        reference_data = ref_f.readlines()
        output_data = test_f.readlines()

    assert reference_data == output_data


@pytest.mark.parametrize(
    "reference_path,input_csv_path",
    [("tests/csv_with_source_reference.csv", "tests/input.csv")],
)
def test_output_csv_with_source(reference_path, input_csv_path, tmp_path):
    """
    Tests write_csv_with_source() function.
    """

    words = get_words_from_csv(input_csv_path, 2)
    spellings = convert_words(words)

    test_path = tmp_path.joinpath("output.csv")

    # pdb.set_trace()
    write_csv_with_source(spellings, test_path, input_csv_path)

    with open(reference_path) as ref_f, open(test_path) as test_f:
        reference_data = ref_f.readlines()
        output_data = test_f.readlines()

    assert reference_data == output_data


@pytest.mark.parametrize(
    "test_input,expected",
    [
        (
            [
                {"word": "apple", "phonetic": "alpha papa papa lima echo"},
                {"word": "APPLE", "phonetic": "ALPHA PAPA PAPA LIMA ECHO"},
                {
                    "word": "P4sSw0rd!",
                    "phonetic": "PAPA 4 sierra SIERRA whiskey 0 romeo delta <exclamation>",  # noqa E501
                },
                {"word": "¬pass", "phonetic": "¬ papa alpha sierra sierra"},
            ],
            [
                "WORD                PHONETIC",
                "----                --------",
                "apple               alpha papa papa lima echo",
                "APPLE               ALPHA PAPA PAPA LIMA ECHO",
                "P4sSw0rd!           PAPA 4 sierra SIERRA whiskey 0 romeo delta <exclamation>",  # noqa E501
                "¬pass               ¬ papa alpha sierra sierra",
            ],
        ),
    ],
)
def test_display_output(test_input, expected, capsys):
    """
    Tests display_output() function.
    """

    display_conversions(test_input)
    captured = capsys.readouterr()
    assert captured.out.splitlines() == expected
