import nox


@nox.session(python=False)
def formatblack(session):
    session.run("poetry", "shell")
    session.run("black", "--include", ".*.py$", ".")


@nox.session(python=False)
def tests(session):
    session.run("poetry", "shell")
    session.run("poetry", "install")
    session.run("pytest", "-vv", "tests/test.py")
